catch2 (3.4.0-2) unstable; urgency=medium

  Catch2 v3 brings significant changes from v2. Is not a (single) header-only
  test tool anymore and requires linking against a library. As such the
  binary package has been renamed libcatch2-dev with a transitional catch2
  package that can be safely removed.

  A note on migration from v2 to v3 is provided by upstream in the package docs [1].
  The gist of it is:
  - Change "pkg-config catch2" to "pkg-config catch2-with-main"
  - Link against Catch2WithMain.
  - Change <catch2/catch.hpp> to <catch2/catch_all.hpp>

  [1] /usr/share/doc/libcatch2-dev/docs/migrate-v2-to-v3.md.gz

 -- Mathieu Mirmont <mat@parad0x.org>  Sun, 19 Nov 2023 21:45:35 +0100
